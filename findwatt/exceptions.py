class MissingInformation(Exception):
    pass


class AlreadyExists(Exception):
    pass


class APIError(Exception):
    pass


class DoesNotExist(Exception):
    pass


class Unauthorized(Exception):
    pass


class NotReady(Exception):
    pass
